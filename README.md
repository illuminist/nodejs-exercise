# Trail Assignment 1 : Basic

1. สมัคร [Dropbox.com](https://www.dropbox.com/)
1. สมัคร [GitLab.com](https://gitlab.com/)
1. ใน Konsoleสร้าง Directory ใหม่ชื่อ nodejs ไว้ใน Home folder ของตัวเอง

        mkdir nodejs
1. เข้าไปยังโฟลเดอร์ที่สร้างใหม่

        cd nodejs
1. ดึงข้อมูลจาก GitLab โดยใช้คำสั่

        git clone https://gitlab.com/illuminist/nodejs-exercise
1. ติดตั้งไฟล์ที่จำเป็นในการรันโปรแกรมฝึกหัด


        npm install
1. Runโปรแกรมฝึกหัด

        npm start
1. ใน web browser ให้พิมพ์ localhost:3000 ใน address bar เพื่อทดลองเข้าดูในโปรแกรมที่รันไว้
1. Runโปรแกรมฝึกหัดโดนโหมดการทดสอบ

        npm test
1. อ่านและสำรวจ code ต่างๆที่อยู่ในโปรแกรม
1. เพิ่มชื่อตัวเองลงในหน้า index ของเว็บที่เปิดผ่านทาง code
